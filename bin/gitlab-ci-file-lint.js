#!/usr/bin/env node
const { run, formatResult } = require("../dist/gitlab-ci-file-lint")
const { config } = require("dotenv")

// Load env files in order of importance (higher == more important)
config({ path: ".env.local" })
config({ path: ".env" })

run()
  .then((result) => {
    console.log(formatResult(result))
    process.exit(result.valid ? 0 : 1)
  })
  .catch((e) => {
    console.error("Could not validate CI file:\n")
    console.error(e)
    process.exit(2)
  })
