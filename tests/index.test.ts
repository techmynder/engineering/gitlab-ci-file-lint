import { run, formatResult } from "../src"
import chalk from "chalk"

// eslint-disable-next-line @typescript-eslint/no-var-requires
jest.mock("node-fetch", () => require("fetch-mock-jest").sandbox())
// eslint-disable-next-line @typescript-eslint/no-var-requires
const fetchMock = require("node-fetch")
fetchMock.config.overwriteRoutes = false

describe("lint runner", () => {
  let originalEnv: NodeJS.ProcessEnv

  beforeEach(() => {
    originalEnv = { ...process.env }
  })

  afterEach(() => {
    process.env = originalEnv

    jest.resetAllMocks()
    fetchMock.reset()
  })

  it.each([
    ["token", () => {}], // eslint-disable-line @typescript-eslint/no-empty-function
    [
      "project id",
      () => {
        process.env.GITLAB_LINT_TOKEN = "token"
      },
    ],
  ])("throws error if %s does not exist", async (_, prepare) => {
    prepare()

    await expect(() => run()).rejects.toThrow(
      "Configuration missing. Define at least the `GITLAB_LINT_TOKEN` and `GITLAB_LINT_PROJECT_ID` environment variables."
    )
    expect(fetchMock).not.toHaveBeenCalled()
  })

  it.each([".gitlab-ci.yml", "custom.yml"])(
    "throws error if file %s does not exist",
    async (file) => {
      const filePath = `__404__/${file}`

      process.env.GITLAB_LINT_TOKEN = "token"
      process.env.GITLAB_LINT_PROJECT_ID = "1"
      process.env.GITLAB_LINT_FILE = filePath

      await expect(() => run()).rejects.toThrow(
        `File "${filePath}" does not exist`
      )

      expect(fetchMock).not.toHaveBeenCalled()
    }
  )

  it.each([401, 500])(
    "throws error if API response status code is unsuccessful (%d)",
    async (code) => {
      process.env.GITLAB_LINT_FILE = "tests/fixtures/.gitlab-ci.yml"
      process.env.GITLAB_LINT_TOKEN = "token"
      process.env.GITLAB_LINT_PROJECT_ID = "1"
      process.env.GITLAB_LINT_BASE_URL = "https://gitlab-custom.org"

      fetchMock.mock(
        "https://gitlab-custom.org/api/v4/projects/1/ci/lint",
        code
      )

      await expect(() => run()).rejects.toThrow(
        `Unexpected response status code "${code}" given`
      )
    }
  )

  it.each([".gitlab-ci.yml", "custom.yml"])(
    "lints file successfully",
    async (file) => {
      const filePath = `tests/fixtures/${file}`

      process.env.GITLAB_LINT_TOKEN = "token"
      process.env.GITLAB_LINT_PROJECT_ID = "1"
      process.env.GITLAB_LINT_FILE = filePath

      fetchMock.mock("https://gitlab.com/api/v4/projects/1/ci/lint", {
        status: "valid",
        errors: [],
        warnings: [],
      })

      expect(await run()).toStrictEqual({
        file: filePath,
        status: "valid",
        errors: [],
        warnings: [],
      })
    }
  )

  it("lints file with issues", async () => {
    const filePath = "tests/fixtures/.gitlab-ci.yml"

    process.env.GITLAB_LINT_FILE = filePath
    process.env.GITLAB_LINT_TOKEN = "token"
    process.env.GITLAB_LINT_PROJECT_ID = "1"

    fetchMock.mock("https://gitlab.com/api/v4/projects/1/ci/lint", {
      status: "invalid",
      errors: ["err1", "err2"],
      warnings: [],
    })

    expect(await run()).toStrictEqual({
      file: filePath,
      status: "invalid",
      errors: ["err1", "err2"],
      warnings: [],
    })
  })

  it("lints file with custom base url", async () => {
    const filePath = "tests/fixtures/.gitlab-ci.yml"

    process.env.GITLAB_LINT_FILE = filePath
    process.env.GITLAB_LINT_TOKEN = "token"
    process.env.GITLAB_LINT_PROJECT_ID = "1"
    process.env.GITLAB_LINT_BASE_URL = "https://gitlab-custom.org"

    fetchMock.mock("https://gitlab-custom.org/api/v4/projects/1/ci/lint", {
      status: "valid",
      errors: [],
      warnings: [],
    })

    expect(await run()).toStrictEqual({
      file: filePath,
      status: "valid",
      errors: [],
      warnings: [],
    })
  })
})

describe("result printer", function () {
  let originalChalkLevel: chalk.Level

  beforeAll(() => {
    originalChalkLevel = chalk.level
    chalk.level = 0
  })

  afterAll(() => {
    chalk.level = originalChalkLevel
  })

  it("prints valid result without errors and warnings", function () {
    expect(
      formatResult({
        file: "file.yml",
        status: "valid",
        errors: [],
        warnings: [],
      })
    ).toStrictEqual(`Validated file.yml: OK`)
  })

  it("prints valid result with no errors but warnings", function () {
    expect(
      formatResult({
        file: "file.yml",
        status: "valid",
        errors: [],
        warnings: ["warning 1", "warning 2"],
      })
    ).toStrictEqual(`Validated file.yml: OK (with warnings)

Warnings:
#1.warning 1
#2.warning 2`)
  })

  it("prints invalid result without errors and warnings", function () {
    expect(
      formatResult({
        file: "file.yml",
        status: "invalid",
        errors: ["error 1", "error 2"],
        warnings: [],
      })
    ).toStrictEqual(`Validated file.yml: FAIL

Errors:
#1.error 1
#2.error 2`)
  })

  it("prints invalid result with errors and no warnings", function () {
    expect(
      formatResult({
        file: "file.yml",
        status: "invalid",
        errors: ["error 1", "error 2"],
        warnings: [],
      })
    ).toStrictEqual(`Validated file.yml: FAIL

Errors:
#1.error 1
#2.error 2`)
  })

  it("prints invalid result with errors and warnings", function () {
    expect(
      formatResult({
        file: "file.yml",
        status: "invalid",
        errors: ["error 1", "error 2"],
        warnings: ["warning 1", "warning 2"],
      })
    ).toStrictEqual(`Validated file.yml: FAIL

Errors:
#1.error 1
#2.error 2

Warnings:
#1.warning 1
#2.warning 2`)
  })
})
