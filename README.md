# GitLab CI File Lint

A small library to help validate GitLab pipeline files using the official API.

## Configuration

Create a `.env` file (or `.env.local` which will have precedence) with the following values:

| Name                     | Description                               | Required                             |
| ------------------------ | ----------------------------------------- | ------------------------------------ |
| `GITLAB_LINT_TOKEN`      | The GitLab access token (`api` scope)     | **Yes**                              |
| `GITLAB_LINT_PROJECT_ID` | The GitLab project ID to validate against | **Yes**                              |
| `GITLAB_LINT_FILE`       | The name of the file to validate          | No _(default: `.gitlab-ci.yml`)_     |
| `GITLAB_LINT_BASE_URL`   | The base URL for the API request          | No _(default: `https://gitlab.com`)_ |

## Example

Using the minimal configuration in `.env`

```
GITLAB_LINT_TOKEN=secret
GITLAB_LINT_PROJECT_ID=1337
```

will validate the root `.gitlab-ci.yml` file.

## Results

The results of the linter will be printed to the console with a matching exit code:

- If the file has **no issues**, the script will exit with an exit code of `0`.
- If the file has **no issues but warnings**, the script will exit with an exit code of `0`.
- If the file has **one or more issues**, the script will exit with an exit code of `1`.
