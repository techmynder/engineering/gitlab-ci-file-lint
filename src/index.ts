import * as fs from "fs"
import fetch from "node-fetch"
import chalk from "chalk"
import { GitLabLintResult, LintResult } from "./types"

export async function run(): Promise<LintResult> {
  const token = process.env.GITLAB_LINT_TOKEN || ""
  const projectId = parseInt(process.env.GITLAB_LINT_PROJECT_ID || "", 10)
  const file = process.env.GITLAB_LINT_FILE || ".gitlab-ci.yml"
  const baseUrl = process.env.GITLAB_LINT_BASE_URL || "https://gitlab.com"

  if (!token || isNaN(projectId)) {
    throw new Error(
      "Configuration missing. Define at least the `GITLAB_LINT_TOKEN` and `GITLAB_LINT_PROJECT_ID` environment variables."
    )
  }

  if (!fs.existsSync(file)) {
    throw new Error(`File "${file}" does not exist`)
  }

  return lintFile(token, projectId, file, baseUrl)
}

/**
 * @see https://docs.gitlab.com/ee/api/lint.html
 */
export async function lintFile(
  token: string,
  projectId: number,
  file: string,
  baseUrl: string
): Promise<LintResult> {
  const response = await fetch(
    `${baseUrl}/api/v4/projects/${projectId}/ci/lint`,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "Private-Token": token,
      },
      body: JSON.stringify({ content: fs.readFileSync(file).toString() }),
    }
  )

  if (response.status !== 200) {
    throw new Error(
      `Unexpected response status code "${response.status}" given`
    )
  }

  return {
    file,
    ...((await response.json()) as GitLabLintResult),
  }
}

const orange = "#ffa500"

const formatLintIssues = (lines: string[]): string =>
  lines.map((line, i) => chalk.bold(`#${i + 1}.`) + line).join("\n")

const formatShortResult = (result: LintResult): string => {
  if (result.status === "valid" && result.warnings.length) {
    return chalk.bold.hex(orange)("OK (with warnings)")
  }

  if (result.status === "invalid" || result.errors.length) {
    return chalk.bold.red("FAIL")
  }

  return chalk.bold.green("OK")
}

export function formatResult(result: LintResult): string {
  let output =
    chalk.bold(`Validated ${result.file}: `) + formatShortResult(result)

  if (result.errors.length) {
    output += chalk.red("\n\nErrors:\n") + formatLintIssues(result.errors)
  }

  if (result.warnings.length) {
    output +=
      chalk.hex(orange)("\n\nWarnings:\n") + formatLintIssues(result.warnings)
  }

  return output
}
