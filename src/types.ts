export type GitLabLintResult = {
  status: "valid" | "invalid"
  errors: string[]
  warnings: string[]
}

export type LintResult = { file: string } & GitLabLintResult
