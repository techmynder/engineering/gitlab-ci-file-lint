module.exports = {
  root: true,
  env: {
    es6: true,
    node: true,
    jest: true,
  },
  plugins: ["@typescript-eslint/eslint-plugin", "prettier"],
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:jest/recommended",
    "prettier",
  ],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    sourceType: "module",
  },
  rules: {
    "@typescript-eslint/no-unused-vars": ["off", { argsIgnorePattern: "^_" }],
    "generator-star-spacing": ["warn", { before: false, after: true }],
    "space-before-function-paren": "off",
    "no-dupe-class-members": "off",
    "no-useless-constructor": "off",
    "@typescript-eslint/no-useless-constructor": "off",
    "prettier/prettier": ["warn"],
    "lines-between-class-members": ["warn", "always"],
    "padding-line-between-statements": [
      "warn",
      { blankLine: "always", prev: "*", next: "return" },
    ],
    "@typescript-eslint/explicit-function-return-type": [
      "warn",
      {
        allowExpressions: true,
        allowTypedFunctionExpressions: true,
      },
    ],
    "@typescript-eslint/explicit-member-accessibility": [
      "warn",
      {
        accessibility: "no-public",
      },
    ],
    "@typescript-eslint/ban-ts-comment": [
      "warn",
      {
        "ts-expect-error": "allow-with-description",
      },
    ],
    "@typescript-eslint/no-non-null-assertion": [2],
  },
  overrides: [
    {
      files: ["*.js", "*.jsx"],
      rules: {
        "@typescript-eslint/explicit-function-return-type": "off",
        "@typescript-eslint/no-var-requires": "off",
      },
    },
  ],
}
