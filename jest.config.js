module.exports = {
  testMatch: [
    "<rootDir>/src/**/?(*.)+(spec|test).[jt]s?(x)",
    "<rootDir>/tests/?(*.)+(spec|test).[jt]s?(x)",
  ],
  testEnvironment: "node",
  moduleDirectories: ["node_modules", "src"],
  collectCoverageFrom: [
    "<rootDir>/src/**",
    "!<rootDir>/src/types.ts",
    "!<rootDir>/tests/**",
  ],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80,
    },
  },
}
